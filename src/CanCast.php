<?php

namespace Eurofirany\CastToClass;

use Illuminate\Support\Collection;

abstract class CanCast
{
    protected array $map = [];

    /**
     * Cast array of items to array of casted objects
     * @param array $data Array of items
     * @param bool $withNewIndexes If true it will be created new array indexes, if false indexes will be stay
     * @return Collection
     */
    public static function castFromArrayOfItems(array $data, bool $withNewIndexes = false): Collection
    {
        foreach ($data as $index => $item) {
            if ($withNewIndexes)
                $newArray[] = self::cast($item);
            else
                $newArray[$index] = self::cast($item);
        }

        return collect($newArray ?? []);
    }

    /**
     * Cast data from array or object to class
     * @param array|object $data
     * @return static
     */
    public static function cast(array|object $data): static
    {
        $class = self::getCurrentClass();

        if (is_array($data))
            $class->castFromArray($data);
        else
            $class->castFromObject($data);

        return $class;
    }

    /**
     * Cast from array data to class
     * @param array $data
     * @return $this
     */
    public function castFromArray(array $data): static
    {
        foreach ($data as $name => $value)
            if ($className = $this->getClassByParam($name))
                $this->{$name} = $this->castFromParamToArrayOrMultiArray($className, $value);
            else
                $this->{$name} = $value;

        return $this;
    }

    /**
     * Cast parameter to array or multi array if have a lot of parameters
     * @param string|CanCast $className Class name to cast
     * @param array $data Data to be casted
     * @return Collection|$this
     */
    public function castFromParamToArrayOrMultiArray(
        string|CanCast $className,
        array $data
    ): Collection|CanCast
    {
        if(is_array(end($data)) || count($data) === 0)
            return $this->castFromMultiArray($className, $data);
        else
            return $className::cast($data);
    }

    /**
     * Cast from array of arrays to array of class objects
     * @param string|CanCast $className
     * @param array $data
     * @return Collection
     */
    public function castFromMultiArray(string|CanCast $className, array $data): Collection
    {
        foreach ($data as $item)
            $newData[] = (new $className())->castFromArray($item);

        return collect($newData ?? []);
    }

    /**
     * Cast from object to class
     * @param object $data
     * @return $this
     */
    public function castFromObject(object $data): static
    {
        foreach (get_object_vars($data) as $name => $value)
            if ($className = $this->getClassByParam($name))
                $this->{$name} = $this->castFromMultiObject($className, $value);
            else
                $this->{$name} = $value;

        return $this;
    }

    /**
     * Cast from array of object to array of class objects
     * @param string|CanCast $className
     * @param array $data
     * @return Collection
     */
    public function castFromMultiObject(string|CanCast $className, array $data): Collection
    {
        foreach ($data as $item)
            $newData[] = (new $className())->castFromObject($item);

        return collect($newData ?? []);
    }

    /**
     * Get called class new instance
     * @return static
     */
    protected static function getCurrentClass(): static
    {
        $class = get_called_class();

        return new $class();
    }

    /**
     * Get class name for map parameter
     * @param string $paramName
     * @return string|null
     */
    protected function getClassByParam(string $paramName): ?string
    {
        if ($className = data_get($this->map, $paramName))
            return $className;

        return null;
    }

    /**
     * Return array version of object
     * @return array
     */
    public function toArray(): array
    {
        return json_decode(json_encode($this), true);
    }

    /**
     * Return collection version of object data
     * @return Collection
     */
    public function toCollection(): Collection
    {
        return collect($this->toArray());
    }

    /**
     * Change collections params to array
     */
    public function paramsToArray(): static
    {
        foreach ($this->map as $field => $class)
            $this->{$field} = $this->mapParamToNewArray($this->{$field});

        return $this;
    }

    /**
     * Map items to array
     * @param CanCast[]|Collection $items Items to be mapped
     * @return array[]
     */
    protected function mapParamToNewArray(Collection|array $items): array
    {
        return $items->map(fn(CanCast $item) => $item->toArray())->toArray();
    }
}